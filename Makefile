debug: ./target/debug/libcurl.rlib
	cargo build

.PHONY: clean
clean:
	cargo clean

coverage: export RUSTFLAGS = -Zinstrument-coverage
coverage: clean
	cargo build
	cargo test -- --nocapture
	grcov . -s . --binary-path ./target/debug/ -t html --branch --ignore-not-existing -o ./target/debug/coverage/
