use std::{
	ffi::{c_void, CString},
	os::raw::{c_char, c_int},
	ptr
};

#[repr(C)] struct CURL { _private: [u8; 0] /* A private field is required for FFI */ }
#[repr(C)] struct curl_slist { _private: [u8; 0] }

// Global CURL codes
const CURL_GLOBAL_DEFAULT: c_int = 3;
const CURLE_OK: c_int = 0;

// CURL opt codes
const CURLOPT_WRITEDATA: c_int = 10001;
const CURLOPT_URL: c_int = 10002;
const CURLOPT_ERRORBUFFER: c_int = 10010;
const CURLOPT_HTTPHEADER: c_int = 10023;
const CURLOPT_HTTP_VERSION: c_int = 84;
const CURLOPT_COPYPOSTFIELDS: c_int = 10165;
const CURLOPT_POSTFIELDSIZE:c_int = 60;
const CURLOPT_PROXY:         c_int = 10004;
const CURLOPT_PROXY_CAINFO: c_int = 10246;
const CURLOPT_CAINFO: c_int = 10065;
const CURLOPT_WRITEFUNCTION: c_int = 20011;
const CURLOPT_POST: c_int = 47;
const CURLOPT_VERBOSE: c_int = 41;

// CURL options
const CURL_HTTP_VERSION_1_1: c_int = 2;

// Curl types
type CURLcode = c_int;
type CURLoption = c_int;

// Curl function bindings
#[link(name = "curl")]
extern {
	fn curl_easy_init() -> *mut CURL;
	fn curl_easy_setopt(handle: *mut CURL, option: CURLoption, value: *mut c_void) -> CURLcode;
	fn curl_easy_cleanup(handle: *mut CURL);
	fn curl_easy_perform(handle: *mut CURL) -> CURLcode;
	fn curl_global_cleanup();
	fn curl_global_init(flags: std::os::raw::c_int) -> CURLcode;
	fn curl_slist_append(list: *mut curl_slist, string: *const c_char) -> *mut curl_slist;
	fn curl_slist_free_all(list: *mut curl_slist);
}

// Curl callback for data retrieving
extern fn callback_writefunction(data: *mut u8, size: usize, nmemb: usize,
	user_data: *mut std::ffi::c_void) -> usize
{
	let slice = unsafe { std::slice::from_raw_parts(data, size * nmemb) };

	let mut vec = unsafe { Box::from_raw(user_data as *mut Vec<u8>) };
	vec.extend_from_slice(slice);
	Box::into_raw(vec);
	nmemb * size
}

pub enum CurlListError {
	CouldNotAppendData
}

struct CurlList {
	handle: *mut curl_slist
}

impl CurlList {
	pub fn new() -> CurlList {
		CurlList { handle: ptr::null_mut() }
	}

	pub fn append(&mut self, value: &str) -> std::result::Result<(),CurlListError> {
		let value_cstr = CString::new(value).unwrap();
		let new_ptr = unsafe { curl_slist_append(self.handle, value_cstr.as_ptr()) };
		if new_ptr.is_null() {
			Err(CurlListError::CouldNotAppendData)
		} else {
			self.handle = new_ptr;
			Ok(())
		}
	}
}

impl Drop for CurlList {
	fn drop(&mut self) {
		if !self.handle.is_null() {
			unsafe { curl_slist_free_all(self.handle) };
		}
	}
}

// Our own error struct
#[derive(Debug)]
pub struct CurlError {
	pub code: CURLcode,
	pub message: String
}

impl CurlError {
	pub fn last(code: CURLcode, buf: &[u8]) -> std::result::Result<(), CurlError> {
		if code == 0 {
			Ok(())
		} else {
			let message = String::from_utf8(buf[0 .. buf.iter().position(|&c| c == 0).unwrap()].to_vec()).unwrap_or("Cannot read curl error: Invalid UTF-8.".into());
			Err(CurlError {	code, message })
		}
	}
}

impl std::convert::From<CurlListError> for CurlError {
	fn from(_e: CurlListError) -> CurlError {
		CurlError {
			code: -1,
			message: "Header list error".into()
		}
	}
}

type Result<T> = std::result::Result<T, CurlError>;

// Our own curl handle
pub struct Curl {
	handle: *mut CURL,
	err_buf: Box<Vec<u8>>,
	data_ptr: *mut Vec<u8>,
	headers: CurlList
}

impl Curl {
	pub fn new() -> std::result::Result<Curl, CurlError> {
		let ret = unsafe { curl_global_init(CURL_GLOBAL_DEFAULT) };
		if ret != 0 {
			return Err(CurlError { code: ret, message: "curl_global_init failed.".to_owned() });
		}

		let handle = unsafe { curl_easy_init() };
		if handle.is_null() { 
            // CURLE_FAILED_INIT according to libcurl-errors(3)
			return Err(CurlError::last(2, "curl_easy_init failed.".as_bytes()).unwrap_err());
		}

		let mut err_buf = Box::new(vec![0u8; 4096]);
		let ret = unsafe {
			curl_easy_setopt(handle, CURLOPT_ERRORBUFFER, err_buf.as_mut().as_mut_ptr() as *mut std::ffi::c_void)
		};
		if ret != 0 {
			// According to the man page, this always returns OK.
			return Err(CurlError::last(-1, "CURLOPT_ERRORBUFFER failed.".as_bytes()).unwrap_err());
		}

		// Set data callback
		let ret = unsafe {
			curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, callback_writefunction as *mut std::ffi::c_void)
		};
		if ret != 0 {
			return Err(CurlError::last(2, "CURLOPT_WRITEFUNCTION failed.".as_bytes()).unwrap_err());
		}

		// Set data pointer
		let data_buf = Box::new(Vec::new());
		let data_ptr = Box::into_raw(data_buf);
		let ret = unsafe {
			curl_easy_setopt(handle, CURLOPT_WRITEDATA, data_ptr as *mut std::ffi::c_void)
		};
		if ret != 0 {
			return Err(CurlError::last(2, "CURLOPT_WRITEDATA failed.".as_bytes()).unwrap_err());
		}

		// Set HTTP version to 1.1, since Kraken does not seem to deal with HTTP/2 properly.
		let ret = unsafe {
			curl_easy_setopt(handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1 as *mut c_void)
		};

		match ret {
			0 => Ok(Curl {
				handle,
				err_buf,
				data_ptr,
				headers: CurlList::new()
			}),
			_ => Err(CurlError::last(2, "CURLOPT_HTTP_VERSION failed.".as_bytes()).unwrap_err())
		}
	}

	fn set_string_opt(&mut self, opt: c_int, value: &str) -> Result<()> {
		let cstr = std::ffi::CString::new(value.as_bytes()).unwrap();
		let ret = unsafe {
			curl_easy_setopt(self.handle, opt, cstr.as_ptr() as *mut std::ffi::c_void)
		};
		CurlError::last(ret, self.err_buf.as_ref())
	}

	pub fn set_url(&mut self, url: &str) -> Result<()> { self.set_string_opt(CURLOPT_URL, url) }

	/// Set the path to the proxy cert.
	pub fn set_proxy_ca_cert_path(&mut self, path: &str) -> Result<()> {
		self.set_string_opt(CURLOPT_PROXY_CAINFO, path)
	}

	pub fn set_proxy(&mut self, proxy: &str) -> Result<()> { self.set_string_opt(CURLOPT_PROXY, proxy) }

	pub fn set_ca_cert(&mut self, path: &str) -> Result<()> { self.set_string_opt(CURLOPT_CAINFO, path) }

	/// This is a helper method to convert a mapping of strings to valid post data.
	pub fn post_map_to_data(map: &std::collections::HashMap<String,String>) -> String {
		map.iter().map(|(k,v)| k.to_owned() + "=" + &v).collect::<Vec<String>>().join("&") + "&"
	}

	/// Enable HTTP POST instead of get.
	/// Data can be an empty string if desired.
	pub fn set_post(&mut self, data: &str) -> Result<()> {
		let ret = unsafe {
			curl_easy_setopt(self.handle, CURLOPT_POST, 1 as *mut std::ffi::c_void)
		};
		if ret != CURLE_OK { return CurlError::last(ret, self.err_buf.as_ref()); }

		let post_data_cstr = std::ffi::CString::new(data.as_bytes()).unwrap();
		let ret = unsafe {
			curl_easy_setopt(self.handle, CURLOPT_POSTFIELDSIZE,
				post_data_cstr.as_bytes().len() as *mut std::ffi::c_void )
		};
		if ret != CURLE_OK {
			return CurlError::last(ret, self.err_buf.as_ref());
		}

		let ret = unsafe {
			curl_easy_setopt(self.handle, CURLOPT_COPYPOSTFIELDS,
				post_data_cstr.as_ptr() as *mut std::ffi::c_void)
		};
		CurlError::last(ret, self.err_buf.as_ref())
	}

	pub fn set_headers(&mut self, headers: &[String]) -> Result<()> {
		self.headers = CurlList::new();
		for h in headers {
			self.headers.append(h)?;
		}

		let ret = unsafe {
			curl_easy_setopt(self.handle, CURLOPT_HTTPHEADER, self.headers.handle as *mut c_void)
		};

		CurlError::last(ret, self.err_buf.as_ref())
	}

	pub fn set_verbose(&mut self, verbose: bool) {
		unsafe {
			curl_easy_setopt(self.handle, CURLOPT_VERBOSE, if verbose { 1 } else { 0 } as *mut c_void)
		};
	}

	pub fn perform(&mut self) -> Result<String> {
		unsafe { &mut *self.data_ptr }.clear();
		let ret = unsafe { curl_easy_perform(self.handle) };
		CurlError::last(ret, self.err_buf.as_ref())
		.map(|_| {
			let b = unsafe { Box::from_raw(self.data_ptr) };
			let data = (*b).clone();
			Box::into_raw(b);
			String::from_utf8(data).unwrap()
		})
	}
}

impl Drop for Curl {
	fn drop(&mut self) {
		unsafe {
			curl_easy_cleanup(self.handle);
			curl_global_cleanup();
			Box::from_raw(self.data_ptr);

			/*
			 * Note: valgrind will complain about blocks of memory still reachable in
			 * cryptographic libraries; see:
			 *
			 * > These are memory blocks that the cryptography libraries chose not to
			 * > release.  Curl has no control over the libraries.  The memory blocks are
			 * > statically allocated, so they are not always considered "memory leaks".
			 * > Valgrind reports them to be conservative.
			 * https://curl.haxx.se/mail/lib-2018-12/0011.html
			 */
		}
	}
}


#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn simple() {
		let mut curl = Curl::new().unwrap();
		assert_eq!(curl.set_url("http://localhost:3000/simple").unwrap(), ());
		curl.set_verbose(true);
		let output = curl.perform();
		assert!(output.is_ok());
		assert_eq!(output.unwrap(), "SimpleResponse");
	}

	#[test]
	fn header() {
		let mut curl = Curl::new().unwrap();
		assert!(curl.set_url("http://localhost:3000/content").is_ok());
		let output1 = curl.perform();
		assert!(output1.is_ok());
		assert_eq!("NO_PARAM", output1.unwrap());

		assert!(curl.set_headers(vec!["test_param: xyz".to_owned()].as_slice()).is_ok());
		let output2 = curl.perform();
		assert!(output2.is_ok());
		assert_eq!("XYZ", output2.unwrap());
	}

	#[test]
	fn post() {
		let mut curl = Curl::new().unwrap();
		curl.set_verbose(true);
		assert!(curl.set_post("").is_ok());
		assert!(curl.set_url("http://localhost:3000/post").is_ok());
		let output1 = curl.perform();
		assert!(output1.is_ok());
		assert_eq!("NO_DATA", output1.unwrap());

		assert!(curl.set_post("data=test").is_ok());
		let output2 = curl.perform();
		assert!(output2.is_ok());
		assert_eq!("TEST", output2.unwrap());
	}
}
